#!/usr/bin/env python

import torch
import torch.nn as nn


class GraphConvolution(nn.Module):
    def __init__(self, indim, outdim, num_relations, num_nodes, num_bases=-1,
                 bias=False):
        """
        Relational Graph Convolutional Layer
        """
        super().__init__()

        self.indim = indim  # no. of input features
        self.outdim = outdim  # no. of output features
        self.num_relations = num_relations  # no. of relations
        self.num_nodes = num_nodes  # no. of nodes (batch size)
        self.num_bases = num_bases

        if self.outdim is not None:
            self.bias = bias
        else:
            self.bias = False

        self.W = None
        self.W_comp = None
        self.b = None

        S = self.num_relations
        if self.num_bases > 0:
            # weights for bases matrices for features
            S = self.num_bases

            self.W_comp = nn.Parameter(torch.empty((self.num_relations,
                                                    self.num_bases)), requires_grad=True)

        self.mask = None
        self.mask_values = None

        # weights for features
        if self.outdim is not None:
            self.W = nn.Parameter(torch.empty((S, self.indim, self.outdim)), requires_grad=True)

            # declare bias
            if self.bias:
                self.b = nn.Parameter(torch.empty(self.outdim), requires_grad=True)
        else:
            self.W = torch.nn.Parameter(torch.empty((self.num_relations, self.indim)), requires_grad=True)
            nn.init.xavier_uniform_(self.W, gain=nn.init.calculate_gain('relu'))

            # declare bias
            if self.bias:
                self.b = nn.Parameter(torch.empty(self.indim), requires_grad=True)

        # initialize weights
        self.init()

    def forward(self, X, A):
        # AXW = AHW

        W = self.W

        if self.outdim is not None and self.num_bases > 0:
            W = torch.einsum('rb,bij->rij', self.W_comp, W)

        if self.outdim is not None:
            FW = torch.einsum('ij,bjk->bik', X, W)
            FW = torch.reshape(FW, (self.num_relations * self.num_nodes, self.outdim))
        else:
            FW = torch.einsum('ij,kj->kij', X, self.W)
            FW = torch.reshape(FW, (self.num_relations * self.num_nodes, self.indim))

        AXW = torch.mm(A, FW)

        if self.bias:
            AXW = torch.add(AXW, self.b)

        return AXW

    def init(self):
        # initialize weights from a uniform distribution following 
        # "Understanding the difficulty of training deep feedforward 
        #  neural networks" - Glorot, X. & Bengio, Y. (2010)
        for name, param in self.named_parameters():
            if name == 'b' or (self.outdim is None and name == 'W'):
                # skip bias
                continue
            nn.init.xavier_uniform_(param, gain=nn.init.calculate_gain('relu'))

        # initialize bias as null vector
        if self.bias:
            nn.init.zeros_(self.b)
